*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
Library           Collections
Library           String

*** Variables ***
${BTN_FIRST_NODE}                   css=a.noUnderline > strong
${CB_FIRST_NODE}                   id=chkAll_436227
${BTN_COPY_TO_SHOPPING_CART}                   id=btnOrder_436227




*** Keywords ***

SW-PRINTMANAGE-ORDER-Select_and_click_the_order_you_wish_to_copy_to_Shopping_Cart
    click element    ${CB_FIRST_NODE}
    click element    ${BTN_COPY_TO_SHOPPING_CART}
    confirm action


SW-PRINTMANAGE-ORDER-AC-Select_and_click_the_order_you_wish_to_display
    click element    ${BTN_FIRST_NODE}



SW-PRINTMANAGE-ORDER-AC-Call_Off_Order_list_will_be_displayed
    page should contain    CALL OFF ORDERS








