*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
Library           Collections
Library           String

*** Variables ***


${WINDOW_DELEVERY_DETAILS}                         name=frmeDD
${FRAME_DELEVERY_ADDRESS}           frmeDA
${LINK_TOP_DELIVER_ADDRESS}                  link=Disneyland Hong Kong - Cinderalla St


${LINK_CREATE_NEW_DELIVER_ADDRES}                   link=CREATE NEW DELIVERY ADDRESS

${TXT_DELIVER_ADDRESS_TITLE}                   id=txtCoyName
${TXT_DELIVER_ADDRESS}                   id=txtAddress
${TXT_DELIVER_CITY}                   id=txtCity
${TXT_DELIVER_POSTCODE}                   id=txtPostCode
${TXT_DELIVER_COUNTRY}                   id=txtCountry
${TXT_DELIVER_CONTAACT_PERSON}                   id=txtContName
${TXT_DELIVER_EMAIL}                   id=txtEmail

${BTN_DELIVER_CREATE}                 id=btnCreate


*** Keywords ***
SW-PRINTMANAGE-DELIVER-ADDRESS-Make_sure_that_new_Delivery_Address_is_created_successfully
    page should contain     NEW DELIVERY ADDRESS HAS BEEN SUCCESSFULLY CREATED

SW-PRINTMANAGE-DELIVER-ADDRESS-Click_Create_New_Delivery_Address_button
    click element    ${BTN_DELIVER_CREATE}
    confirm action

SW-PRINTMANAGE-DELIVER-ADDRESS-Enter_Delivery_Address_details
    select frame    frmeDA
    ${Random_text} =  Generate Random String  6  [UPPER]
    ${Random_number} =  Generate Random String  4   [NUMBERS]
    SW-PRINTMANAGE-DELIVER-ADDRESS-enter_address_title    test title ${Random_text}
    SW-PRINTMANAGE-DELIVER-ADDRESS-enter_address    test address line ${Random_text}
    SW-PRINTMANAGE-DELIVER-ADDRESS-enter_city    Sydney
    SW-PRINTMANAGE-DELIVER-ADDRESS-enter_postal_code    2000
    SW-PRINTMANAGE-DELIVER-ADDRESS-enter_country     Australia
    SW-PRINTMANAGE-DELIVER-ADDRESS-enter_contact_person    test name ${Random_text}
    SW-PRINTMANAGE-DELIVER-ADDRESS-enter_email    idmt110+${Random_number}@gmail.com



SW-PRINTMANAGE-DELIVER-ADDRESS-enter_email
    [Arguments]    ${email}
    input text    ${TXT_DELIVER_EMAIL}     ${email}

SW-PRINTMANAGE-DELIVER-ADDRESS-enter_contact_person
    [Arguments]    ${cp}
    input text    ${TXT_DELIVER_CONTAACT_PERSON}     ${cp}

SW-PRINTMANAGE-DELIVER-ADDRESS-enter_country
    [Arguments]    ${ctry}
    input text    ${TXT_DELIVER_COUNTRY}     ${ctry}

SW-PRINTMANAGE-DELIVER-ADDRESS-enter_postal_code
    [Arguments]    ${pc}
    input text    ${TXT_DELIVER_POSTCODE}     ${pc}

SW-PRINTMANAGE-DELIVER-ADDRESS-enter_city
    [Arguments]    ${city}
    input text    ${TXT_DELIVER_CITY}     ${city}

SW-PRINTMANAGE-DELIVER-ADDRESS-enter_address
    [Arguments]    ${add}
    input text    ${TXT_DELIVER_ADDRESS}     ${add}

SW-PRINTMANAGE-DELIVER-ADDRESS-enter_address_title
    [Arguments]    ${title}
    input text    ${TXT_DELIVER_ADDRESS_TITLE}     ${title}


SW-PRINTMANAGE-DELIVER-ADDRESS-Click_Create_Delivery_Address_tab
    click link    ${LINK_CREATE_NEW_DELIVER_ADDRES}


SW-PRINTMANAGE-DELIVER-ADDRESS-Make_sure_that_Delivery_Address_list_and_details_are_displayed
    select frame     frmeDD
    current frame contains     UPDATE DELIVERY ADDRESS - DISNEYLAND HONG KONG
    current frame contains     Delivery Address Details

SW-PRINTMANAGE-DELIVER-ADDRESS-Select_and_Click_any_Delivery_Address
    select frame    ${FRAME_DELEVERY_ADDRESS}
    click link    ${LINK_TOP_DELIVER_ADDRESS}