*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
Library           Collections
Library           String

*** Variables ***
${TXT_CONTACT_NAME}                   id=txtContactName
${CALANDER}                   id=tcalico_0
${CALANDER_NEXT_MONTH}                   css=td[title="Next Month"]
${CALANDER_NEXT_MONTH_DATE}                  //td[@onclick="A_TCALS['0'].f_hide(1473186600000)"]
${CALANDER_CURRENT_DATE}              css=td.selected
${TXT_PO_NUMBER}                id=txtPONum
${TXT_NOTE}                id=txtNote
${BTN_SUBMIT}                id=btnSubmit

${TXT_SUBMIT_DATE}                txtReqDelDate





*** Keywords ***

SW-PRINTMANAGE-CARTVIEW-Enter_the_delivery_information_and_click_on_Submit_Shopping_Cart
    SW-PRINTMANAGE-CARTVIEW-Enter_Contact_name    test name
    click element     ${CALANDER}
    click element     ${CALANDER_CURRENT_DATE}
    #click element     ${CALANDER_NEXT_MONTH}
    #click element     ${CALANDER_NEXT_MONTH_DATE}
    #input text   ${TXT_SUBMIT_DATE}    16/09/2016
    SW-PRINTMANAGE-CARTVIEW-Enter_pono    123456
    SW-PRINTMANAGE-CARTVIEW-Enter_note    test note
    click element     ${BTN_SUBMIT}
    confirm action


SW-PRINTMANAGE-CARTVIEW-Enter_note
    [Arguments]    ${note}
    input text    ${TXT_NOTE}     ${note}

SW-PRINTMANAGE-CARTVIEW-Enter_pono
    [Arguments]    ${pono}
    input text    ${TXT_PO_NUMBER}     ${pono}

SW-PRINTMANAGE-CARTVIEW-Enter_Contact_name
    [Arguments]    ${cname}
    input text    ${TXT_CONTACT_NAME}     ${cname}