*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
Library           Collections
Library           String

*** Variables ***
${BTN_JOB_FIRST_JOB}                    css=a.noUnderline > strong
${BTN_JOB_SHOW_ALL}                    id=btnShowAll



*** Keywords ***
SW-PRINTMANAGE-JOBS-Show_all_and_Select_the_job number_you_wish
    click element    ${BTN_JOB_SHOW_ALL}
    click element    ${BTN_JOB_FIRST_JOB}

SW-PRINTMANAGE-JOBS-verify_dispach_details
    page should contain    Despatch Details

SW-PRINTMANAGE-JOBS-Get_the_job_status_for
    page should contain    Job Details

SW-PRINTMANAGE-JOBS-Select_the_job number_you_wish
    click element    ${BTN_JOB_FIRST_JOB}
