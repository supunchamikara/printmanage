*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
Library           Collections
Library           String

*** Variables ***
${BTN_SSR_CALANDER}                         css=i.icon-calendar
${BTN_SSR_CALANDER_YEAR}                         css=span.year.active
${BTN_SSR_CALANDER_MONTH}                         css=span.month.active
${TXT_SSR_CALANDER_MONTH}                        id=monthPicker2
${TXT_SSR_MONTH_TO_DISPLAY}                         id=monthsToDisplay
${BTN_SSR_REPORT}                     id=DoReportButton


*** Keywords ***

SW-PRINTMANAGE-REPORT-SSR-Click_Create_Report_button
    click element    ${BTN_SSR_REPORT}

SW-PRINTMANAGE-REPORT-SSR-Enter_months_to_show
    click element    ${TXT_SSR_CALANDER_MONTH}
    SW-PRINTMANAGE-REPORT-SSR-enter_month    1

SW-PRINTMANAGE-REPORT-SSR-enter_month
    [Arguments]    ${month}
    input text    ${TXT_SSR_MONTH_TO_DISPLAY}     ${month}

SW-PRINTMANAGE-REPORT-SSR-Indicate_Starting_Month_and_Year
    click element    ${BTN_SSR_CALANDER}
    click element    ${BTN_SSR_CALANDER_YEAR}
    #click element    ${BTN_SSR_CALANDER_MONTH}