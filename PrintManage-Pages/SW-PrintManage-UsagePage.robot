*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
Library           Collections
Library           String

*** Variables ***
${RB_PIE_CHART}                    xpath=//input[@id='rdChart' and @value='pie']
${RB_COLUMN_CHART}                  id=rdChart

${BTN_YEAR}                        id=btnYear_2016
${BTN_MONTH}                        id=btnMonth_2

*** Keywords ***

SW-PRINTMANAGE-USAGE-Select_Column_Chart
    Wait Until Page Contains Element    ${RB_COLUMN_CHART}
    click element    ${RB_COLUMN_CHART}

SW-PRINTMANAGE-USAGE-Click_any_Month_Button
    select frame    frmeMS
    click element    ${BTN_MONTH}

SW-PRINTMANAGE-USAGE-Click_any_Year_button
    click element    ${BTN_YEAR}

SW-PRINTMANAGE-USAGE-Select_Pie_Chart
    Wait Until Page Contains Element    ${RB_PIE_CHART}
    click element    ${RB_PIE_CHART}

