*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
Library           Collections
Library           String

*** Variables ***
${TXT_JOB_TITLE}                        id=txtJobTitle
${TXT_CUTOMER_CODE}                     id=txtCustStockRef
${TXT_QTY1}                             id=txtQtys1
${DDL_SET_FLAG_SIZE}                    id=selFlatSize
${DDL_FOLDING_STYLE}                        id=selFoldingStyle
${DDL_FIN_SIZE}                   id=selFinSize
${DDL_STOCK_TYPE}                   id=selStockType
${DDL_SIDE}                   id=selSide
${DDL_COLOR_FONT1}                   id=selColoursFront1
${DDL_VARNISH}                   id=selVarnish
${DDL_EMBILISHMENT}                   id=selEmbellishment
${DDL_FINISHING}                  id=selFinishing
${DDL_IMAGE_BLEED}                   id=selImageBleeds
${DDL_PACKING}                   id=selPacking
${TXT_ADD_NOTES}                             id=txtAddNotes
${CALANDER}                       id=tcalico_0
${CALANDER_DATE}                css=td.today.selected
#//td[@onclick="A_TCALS['0'].f_hide(1472581800000)"]
${DDL_DELIVER_DETAILS}                       id=selDelAdd
${TXT_ADDITIONAL_DILIVERY_INSTRUCTION}                       id=txtAddDelInst
${BTN_REQUEST_QUOTE}                       id=btnRequest
${QUOTE_VIEW_WINDOW}                                    title=Fuji Xerox DMS Print Management Solution : BLUE WATER - Quote Details




*** Keywords ***

SW-PRINTMANAGE-QUOTE-REQUEST-Make_sure_that_the_quote_list_and_details_are_displayed
    Select Window    ${QUOTE_VIEW_WINDOW}    # Focus popup window
    Wait Until Page Contains     Quote Details -


SW-PRINTMANAGE-QUOTE-REQUEST-Click_Request_Quote_button
    click element    ${BTN_REQUEST_QUOTE}
    confirm action


SW-PRINTMANAGE-QUOTE-REQUEST-Complete_the_Quote_Request_form
    SW-PRINTMANAGE-QUOTE-REQUEST-Enter_job_title    test title
    SW-PRINTMANAGE-QUOTE-REQUEST-Enter_customer_code    test stock ref
    SW-PRINTMANAGE-QUOTE-REQUEST-Enter_qty    1
    SW-PRINTMANAGE-QUOTE-REQUEST-Select_flag_size    99 x 210mm
    SW-PRINTMANAGE-QUOTE-REQUEST-Select_folding_style    None
    SW-PRINTMANAGE-QUOTE-REQUEST-Select_fin_size    99 x 210mm
    SW-PRINTMANAGE-QUOTE-REQUEST-Select_stock_type    Matt Art 100gsm
    SW-PRINTMANAGE-QUOTE-REQUEST-Select_side    1 Side Only
    SW-PRINTMANAGE-QUOTE-REQUEST-Select_color_font    BLACK
    SW-PRINTMANAGE-QUOTE-REQUEST-Select_varnish    None
    SW-PRINTMANAGE-QUOTE-REQUEST-Select_Embellishment    None
    SW-PRINTMANAGE-QUOTE-REQUEST-Select_Finishing    None
    SW-PRINTMANAGE-QUOTE-REQUEST-Select_ImageBleeds    No
    SW-PRINTMANAGE-QUOTE-REQUEST-Select_Packing_type    Pack loose in cartons
    SW-PRINTMANAGE-QUOTE-REQUEST-Enter_Additional_Notes    addn noites
    click element     ${CALANDER}
    click element     ${CALANDER_DATE}
    SW-PRINTMANAGE-QUOTE-REQUEST-Select_delivery_details    Fuji Xerox DMS - 26 College St
    SW-PRINTMANAGE-QUOTE-REQUEST-Enter_Additional_delivery_instruction    fdgdf




SW-PRINTMANAGE-QUOTE-REQUEST-Enter_Additional_delivery_instruction
    [Arguments]    ${adn}
    input text    ${TXT_ADDITIONAL_DILIVERY_INSTRUCTION}     ${adn}


SW-PRINTMANAGE-QUOTE-REQUEST-Select_delivery_details
    [Arguments]    ${da}
    Select From List    ${DDL_DELIVER_DETAILS}    ${da}

SW-PRINTMANAGE-QUOTE-REQUEST-Enter_Additional_Notes
    [Arguments]    ${an}
    input text    ${TXT_ADD_NOTES}     ${an}

SW-PRINTMANAGE-QUOTE-REQUEST-Select_Packing_type
    [Arguments]    ${Packing}
    Select From List    ${DDL_PACKING}    ${Packing}

SW-PRINTMANAGE-QUOTE-REQUEST-Select_Packing
    [Arguments]    ${Packing}
    Select From List    ${DDL_PACKING}    ${Packing}

SW-PRINTMANAGE-QUOTE-REQUEST-Select_ImageBleeds
    [Arguments]    ${ImageBleeds}
    Select From List    ${DDL_IMAGE_BLEED}    ${ImageBleeds}

SW-PRINTMANAGE-QUOTE-REQUEST-Select_Finishing
    [Arguments]    ${Finishing}
    Select From List    ${DDL_FINISHING}    ${Finishing}

SW-PRINTMANAGE-QUOTE-REQUEST-Select_Embellishment
    [Arguments]    ${Embellishment}
    Select From List    ${DDL_EMBILISHMENT}    ${Embellishment}

SW-PRINTMANAGE-QUOTE-REQUEST-Select_varnish
    [Arguments]    ${v}
    Select From List    ${DDL_VARNISH}    ${v}

SW-PRINTMANAGE-QUOTE-REQUEST-Select_color_font
    [Arguments]    ${cf}
    Select From List    ${DDL_COLOR_FONT1}    ${cf}

SW-PRINTMANAGE-QUOTE-REQUEST-Select_side
    [Arguments]    ${side}
    Select From List    ${DDL_SIDE}    ${side}

SW-PRINTMANAGE-QUOTE-REQUEST-Select_stock_type
    [Arguments]    ${sty}
    Select From List    ${DDL_STOCK_TYPE}    ${sty}

SW-PRINTMANAGE-QUOTE-REQUEST-Select_fin_size
    [Arguments]    ${fis}
    Select From List    ${DDL_FIN_SIZE}    ${fis}

SW-PRINTMANAGE-QUOTE-REQUEST-Select_folding_style
    [Arguments]    ${fs}
    Select From List    ${DDL_FOLDING_STYLE}    ${fs}

SW-PRINTMANAGE-QUOTE-REQUEST-Select_flag_size
    [Arguments]    ${fs}
    Select From List    ${DDL_SET_FLAG_SIZE}    ${fs}


SW-PRINTMANAGE-QUOTE-REQUEST-Enter_qty
    [Arguments]    ${qty}
    input text    ${TXT_QTY1}     ${qty}

SW-PRINTMANAGE-QUOTE-REQUEST-Enter_customer_code
    [Arguments]    ${cc}
    input text    ${TXT_CUTOMER_CODE}     ${cc}

SW-PRINTMANAGE-QUOTE-REQUEST-Enter_job_title
    [Arguments]    ${jt}
    input text    ${TXT_JOB_TITLE}     ${jt}



















