*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
Library           Collections
Library           String

*** Variables ***
${XXX}                   css=a.noUnderline > strong





*** Keywords ***

SW-PRINTMANAGE-ORDER-UA-Unapproved_Order_list_will_be_displayed
    page should contain    UNAPPROVED ORDER LIST

