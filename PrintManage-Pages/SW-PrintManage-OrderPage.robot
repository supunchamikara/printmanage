*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
Library           Collections
Library           String

*** Variables ***
${BTN_BROCHER}                   //div[@id='floatMenu']/table[3]/tbody/tr/td
${TXT_QTY}                  id=txtQty_0
${BTN_ADD}                 id=btnAdd_0
${TXT_POSTER_QTY}                  id=txtQty
${BTN_POSTER_ADD}                 id=btnAdd
${BTN_PROCEED_TO_ORDER}                 css=a > img
${BTN_POSTER}                id=img_0
${BTN_BOOK}                     css=tr.trTitle > td
${BTN_FORM}                     //div[@id='floatMenu']/table[7]/tbody/tr/td
${BTN_ENVILOP}                     //div[@id='floatMenu']/table[5]/tbody/tr/td
${BTN_KIT}                   //div[@id='floatMenu']/table[9]/tbody/tr/td
${BTN_LETTERHEAD}                   //div[@id='floatMenu']/table[11]/tbody/tr/td
${BTN_WITH_COMP_SLIP}              //div[@id='floatMenu']/table[13]/tbody/tr/td
${BTN_REMOVE_FROM_CART}                 id=btnRemove_0


*** Keywords ***
SW-PRINTMANAGE-ORDER-Click_OK
    confirm action

SW-PRINTMANAGE-ORDER-Click_Update_Qty_button
    click button     ${BTN_ADD}
    #confirm action

SW-PRINTMANAGE-ORDER-Edit_the_quatity_value
    SW-PRINTMANAGE-ORDER-Enter_qty    2




SW-PRINTMANAGE-ORDER-click_on_with_comp_slip
    click element       ${BTN_WITH_COMP_SLIP}

SW-PRINTMANAGE-ORDER-click_on_letterhead
    click element       ${BTN_LETTERHEAD}

SW-PRINTMANAGE-ORDER-click_on_kit
    click element       ${BTN_ENVILOP}

SW-PRINTMANAGE-ORDER-click_on_envilope
    click element       ${BTN_ENVILOP}

SW-PRINTMANAGE-ORDER-click_on_form
    click element       ${BTN_FORM}

SW-PRINTMANAGE-ORDER-click_on_book
    click element       ${BTN_BOOK}

SW-PRINTMANAGE-ORDER-Click_Remove_button_and_confirm_action
    click element    ${BTN_REMOVE_FROM_CART}
    confirm action

SW-PRINTMANAGE-ORDER-Poster_Frame_Enter_the_quantity_you_wish_to_order_then_click_on_Add_to_Cart
    select frame    frmeOD_0
    SW-PRINTMANAGE-ORDER-Enter_poster_frame_qty    1
    click button     ${BTN_POSTER_ADD}
    confirm action

SW-PRINTMANAGE-ORDER-click_on_poster
    click element     ${BTN_POSTER}

SW-PRINTMANAGE-ORDER-Click_Proceed_to_Order_button
    click element    ${BTN_PROCEED_TO_ORDER}


SW-PRINTMANAGE-ORDER-Enter_the_quantity_you_wish_to_order_then_click_on_Add_to_Cart
    SW-PRINTMANAGE-ORDER-Enter_qty    1
    click button     ${BTN_ADD}
    confirm action
    #choose ok on next confirmation

SW-PRINTMANAGE-ORDER-Enter_poster_frame_qty
    [Arguments]    ${pqty}
    input text    ${TXT_POSTER_QTY}     ${pqty}

SW-PRINTMANAGE-ORDER-Enter_qty
    [Arguments]    ${qty}
    input text    ${TXT_QTY}     ${qty}



SW-PRINTMANAGE-ORDER-Click_Brochure
    click element    ${BTN_BROCHER}
