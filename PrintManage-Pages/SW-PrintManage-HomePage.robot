*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
Library           Collections
Library           String

*** Variables ***
${LOGIN URL}                   link=Sign in



*** Keywords ***
SW-PRINTMANAGE_LOGIN-Log_into_the_Print_Management_Portal
    SW-PRINTMANAGE_LOGIN-Open_Browser_To_Login_Page
    click link    ${LOGIN URL}

SW-PRINTMANAGE_LOGIN-Open_Browser_To_Login_Page
    #Open Browser    ${SERVER}    ${BROWSER}    remote_url=${REMOTE_URL}    desired_capabilities=${DESIRED_CAPABILITIES}
	Open Browser    ${SERVER}    ${BROWSER}    None     remote_url=${REMOTE_URL}
    Maximize Browser Window
    Set Selenium Speed    ${DELAY}