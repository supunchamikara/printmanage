*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
Library           Collections
Library           String

*** Variables ***
#${BTN_ADD_NEW_ORDER}                    css=input.mandatory


*** Keywords ***
#SW-PRINTMANAGE-PRINT-From_the_navigation_menu_click_print_on_demad
    #click link    ${LINK_STOCK_PRINT_ON_DEMAND}