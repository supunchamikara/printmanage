*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
Library           Collections
Library           String

*** Variables ***



*** Keywords ***

SW-PRINTMANAGE-REPORT-QUOTE-Make_sure_that_stock_usage_mothlu_Report_is_displayed
    wait until page contains     BLUE WATER - STOCK MONTHLY USAGE