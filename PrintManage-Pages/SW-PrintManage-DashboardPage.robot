*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
Library           Collections
Library           String

*** Variables ***
${BTN_PRINT_MANAGEMENT_TAB}                   ${DOMAIN}/fxdms/authenticate



*** Keywords ***
SW-PRINTMANAGE-DASHBOARD-Go_to_print_management_page
    go to      ${BTN_PRINT_MANAGEMENT_TAB}