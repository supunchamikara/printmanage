*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
Library           Collections
Library           String

*** Variables ***
${BTN_ORDERCD_FIRST_NODE}                   css=a.noUnderline > strong
${CB_ORDERCD_FIRST_NODE}                   id=chk_436105_0
${BTN_ORDERCD_COPY_TO_SHOPPING_CART}                   id=btnOrder_436105
${BTN_SHOW_ALL}                   id=btnShowAll
${BTN_FIRST_NODE_LINK}                      css=a.noUnderline > strong
${BTN_VIEW_NODE_DETAILS}          link=BLU009 Blue Water Carbonless Books




*** Keywords ***

SW-PRINTMANAGE-ORDER-CD-Select_and_click_the_order_you_wish_to_display_order
    click element     ${BTN_SHOW_ALL}
    click element     ${BTN_FIRST_NODE_LINK}
    click element     ${BTN_VIEW_NODE_DETAILS}



SW-PRINTMANAGE-ORDER-CD-Closed_Order_list_will_be_displayed
    page should contain    CLOSED ORDERS

SW-PRINTMANAGE-ORDER-CD-Select_and_click_the_order_you_wish_to_copy_to_Shopping_Cart
    click element    ${CB_ORDERCD_FIRST_NODE}
    click element    ${BTN_ORDERCD_COPY_TO_SHOPPING_CART}
    confirm action


SW-PRINTMANAGE-ORDER-CD-Select_and_click_the_order_you_wish_to_display
    click element    ${BTN_ORDERCD_FIRST_NODE}



SW-PRINTMANAGE-ORDER-CD-Dispached_Order_list_will_be_displayed
    page should contain    DESPATCHED ORDERS