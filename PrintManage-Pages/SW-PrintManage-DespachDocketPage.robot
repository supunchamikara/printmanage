*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
Library           Collections
Library           String

*** Variables ***
${LINK_FIRST_NODE}                   link=448660
#${LINK_FIRST_NODE}                   xpath=//a[@class='noUnderline' and @text='448660']
${LINK_DISPACH_DOCKET_NO}           css=a.noUnderline > img



*** Keywords ***
SW-PRINTMANAGE-DESPACH-DOCKET-Select_the_docket_number_you_wish_to_view
    click element    ${LINK_DISPACH_DOCKET_NO}

SW-PRINTMANAGE-DESPACH-DOCKET-Select_and_click_the_order_you_wish_to_display
    click element    ${LINK_DISPACH_DOCKET_NO}

SW-PRINTMANAGE-DESPACH-DOCKET-Dispatch_Docket_Sales_Order_list_will_be_displayed
    page should contain    DESPATCH DOCKET
