*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
Library           Collections
Library           String

*** Variables ***
${BTN_CC_FIRST_QUOTE}                    id=btnSave_0
${BTN_CC_SAVE}                    id=btnRequest




*** Keywords ***
SW-PRINTMANAGE-CREATE-QUOTE-Make_sure_that_quote_request_is_saved-quote number is generated
    wait until page contains     Your Quote Request has been sent.

SW-PRINTMANAGE-CREATE-QUOTE-Click_Save_button
    click element     ${BTN_CC_SAVE}
    confirm action


SW-PRINTMANAGE-CREATE-QUOTE-Select_quote_and_click_Go_button
    click element     ${BTN_CC_FIRST_QUOTE}