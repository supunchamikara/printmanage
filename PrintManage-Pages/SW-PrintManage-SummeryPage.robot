*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
Library           Collections
Library           String

*** Variables ***
${BTN_ADD_NEW_ORDER}                            css=input.mandatory
${LINK_ORDERING}                                link=Ordering
${LINK_ORDERING_CALL_OF_ORDERING}               link=Call off orders
${LINK_ORDERING_DISPACHED_ORDERS}               link=Despatched orders
${LINK_ORDERING_CLOSED_ORDERS}                  link=Closed orders
${LINK_ORDERING_UNAPPROVED_ORDERS}              link=Unapproved orders

${LINK_QUOTING}                                 link=Quoting
${LINK_DISPACH_DOCKET}                          link=Despatch docket
${LINK_CATELOG_ORDER}                           link=Catalogue order
${LINK_RFQFORM_BROCHERS_FLYERS}                 link=RFQ form - Brochures/Flyers - Flat or Folded only (No Binding)
${LINK_QUOTING_CONFIRMED_LOST_QUOTE}            link=Confirmed/Lost quote
${LINK_QUOTING_LIVE_QUOTE}                      link=Live quote
${LINK_QUOTING_CREATE_QUOTE}                    link=Create Quote from Product

${LINK_JOBS_JOBS}                               link=Jobs
${LINK_JOBS_LIVE_JOB}                           link=Live jobs
${LINK_JOBS_JOBS_ON_PROOF}                      link=Jobs on proof
${LINK_JOBS_DISPACHED_JOBS}                     link=Despatched jobs
${LINK_JOBS_CLOSED_JOBS}                        link=Closed jobs
${LINK_JOBS_DISPACHED_DOCKET}                        css=a[href='despatch_docket_job.asp']
${LINK_JOBS_HELD_JOBS}                          link=Held jobs


${LINK_STOCK}                                   link=Stock/Category
${LINK_STOCK_CATEGORY_INQ}                      link=Stock/Category enquiry
${LINK_STOCK_PRINT_ON_DEMAND}                   link=Print on demand enquiry
${LINK_STOCK_LOW_STOCK_LEVEL}                   link=Low stock levels

${LINK_ACCOUNT}                                 link=Account
${LINK_ACCOUNT_SPEND_USAGE}                     link=Spend/Usage
${LINK_ACCOUNT_DELIVERABLE}                     link=Deliverables

${LINK_REPORT}                                  link=Report
${LINK_REPORT_JOB_REPORT}                       link=Job report
${LINK_REPORT_QUOTE_REPORT}                     link=Quote report
${LINK_REPORT_STOCK_FORECAST_REPORT}            link=Stock forecast
${LINK_REPORT_STOCK_USGE_MONTHLY_REPORT}            link=Stock usage monthly
${LINK_REPORT_SALES_ORDER_REPORT}               link=Sales order report
${LINK_REPORT_USAGE_BY_QTY}                     link=Usage by quantity
${LINK_REPORT_USAGE_BY_VALUE}                     link=Usage by value
${LINK_REPORT_STOCK_ON_ORDER}                     link=Stock on order
${LINK_REPORT_EXPENDITURE_BY}                     link=Expenditure by profile


${LINK_DELIVER_ADDRESS}                         link=Delivery addresses

*** Keywords ***

SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_held_jobs
    click link    ${LINK_JOBS_HELD_JOBS}

SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_Deliverables
    click link    ${LINK_ACCOUNT_DELIVERABLE}


SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_Expenditure_by_profile_report
    click link    ${LINK_REPORT_EXPENDITURE_BY}

SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_stock_on_order_report
    click link    ${LINK_REPORT_STOCK_ON_ORDER}

SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_Usage_by_value_report
    click link    ${LINK_REPORT_USAGE_BY_VALUE}

SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_delivery_address
    click link    ${LINK_DELIVER_ADDRESS}

SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_usage_by_qty
    click link    ${LINK_REPORT_USAGE_BY_QTY}

SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_sales_orde_repot
    click link    ${LINK_REPORT_SALES_ORDER_REPORT}

SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_job_report
    click link    ${LINK_REPORT_JOB_REPORT}

SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_quote_report
    click link    ${LINK_REPORT_QUOTE_REPORT}

SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_stock_forecast_report
    click link    ${LINK_REPORT_STOCK_FORECAST_REPORT}

SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_stock_usage_mothly_report
    click link    ${LINK_REPORT_STOCK_USGE_MONTHLY_REPORT}

SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_report
    click link    ${LINK_REPORT}

SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_spend_usage
    click link    ${LINK_ACCOUNT_SPEND_USAGE}

SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_account
    click link    ${LINK_ACCOUNT}

SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_low_stock_level
    click link    ${LINK_STOCK_LOW_STOCK_LEVEL}

SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_print_on_demad
    click link    ${LINK_STOCK_PRINT_ON_DEMAND}

SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_category_inquiry
    click link    ${LINK_STOCK_CATEGORY_INQ}

SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_stock
    click link    ${LINK_STOCK}

SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_live_jobs
    click link    ${LINK_JOBS_LIVE_JOB}


SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_jobs_on_proof
    click link    ${LINK_JOBS_JOBS_ON_PROOF}

SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_dispached_jobs
    click link    ${LINK_JOBS_DISPACHED_JOBS}


SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_dispached_docket
    click link    ${LINK_JOBS_DISPACHED_DOCKET}





SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_closed_jobs
    click link    ${LINK_JOBS_CLOSED_JOBS}


SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_jobs
    click link    ${LINK_JOBS_JOBS}

SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_Create_Quote_from_Product
    click link    ${LINK_QUOTING_CREATE_QUOTE}

SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_live_quote
    click link    ${LINK_QUOTING_LIVE_QUOTE}

SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_Confirmed_Lost_quote
    click link    ${LINK_QUOTING_CONFIRMED_LOST_QUOTE}

SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_RFQ form - Brochure / Flyers with out Binding
    click link    ${LINK_RFQFORM_BROCHERS_FLYERS}

SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_Catalogue Order
    click link    ${LINK_CATELOG_ORDER}


SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_Dispatch_Docket
    click link    ${LINK_DISPACH_DOCKET}

SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_on_quoting
    click link    ${LINK_QUOTING}



SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_on_unapproved_orders
    click link    ${LINK_ORDERING_UNAPPROVED_ORDERS}


SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_on_closed_orders
    click link    ${LINK_ORDERING_CLOSED_ORDERS}

SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_on_dispached_orders
    click link    ${LINK_ORDERING_DISPACHED_ORDERS}


SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_oncall_of_Ordering
    click link    ${LINK_ORDERING_CALL_OF_ORDERING}

SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_on_Ordering
    click link    ${LINK_ORDERING}


SW-PRINTMANAGE-SUMMERY-Click_Add_New_Order
    click element    ${BTN_ADD_NEW_ORDER}