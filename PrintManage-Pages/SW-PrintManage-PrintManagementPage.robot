*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
Library           Collections
Library           String

*** Variables ***
${RB_CUSTOMER}                   id=rdType
${DDL_SALES_REP}                   id=selRep
${BTN_GO}                     id=btnSubmit
${LINK_BLUEWATER}                    link=BLUE WATER



*** Keywords ***
SW-PRINTMANAGE-PM-Select_Blue_Water_as_client
    click link    ${LINK_BLUEWATER}


SW-PRINTMANAGE-PM-Select_Customer_then_select_'Test Rep'_as_Sales_Rep
    click element     ${RB_CUSTOMER}
    SW-PRINTMANAGE-PM-Select_sales_rep    _Test Rep
    click element      ${BTN_GO}

SW-PRINTMANAGE-PM-Select_sales_rep
    [Arguments]    ${sr}
    Select From List    ${DDL_SALES_REP}     ${sr}
