*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
Library           Collections
Library           String

*** Variables ***



*** Keywords ***

SW-PRINTMANAGE-REPORTJOB-Make_sure_that_Job_Report_is_displayed
    wait until page contains     BLUE WATER - JOBS IN PROGRESS


