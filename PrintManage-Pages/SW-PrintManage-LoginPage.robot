*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
Library           Collections
Library           String

*** Variables ***
${TXT_USERNAME}                   id=username
${TXT_PASSWORD}                   id=password
${BTN_SIGNIN}                     css=button.btn.btn-primary


*** Keywords ***
SW-PRINTMANAGE-LOGIN-Sign_in
    SW-PRINTMANAGE-Enter_username    idmt110@gmail.com
    SW-PRINTMANAGE-Enter_password    Pa$$w0rd
    click element      ${BTN_SIGNIN}
    sleep    3s


SW-PRINTMANAGE-Enter_password
    [Arguments]    ${pwd}
    input text    ${TXT_PASSWORD}     ${pwd}

SW-PRINTMANAGE-Enter_username
    [Arguments]    ${uname}
    input text    ${TXT_USERNAME}     ${uname}