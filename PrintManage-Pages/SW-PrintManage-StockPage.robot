*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
Library           Collections
Library           String

*** Variables ***
${BTN_STOCK_BROCKER}                    //div[@id='floatMenu']/table[3]/tbody/tr/td


*** Keywords ***

SW-PRINTMANAGE-STOCK-From_the_navigation_menu_click_stock_brocher
    click element     ${BTN_STOCK_BROCKER}