*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
Library           Collections
Library           String

*** Variables ***
${LINK_VIEW}                    link=View
${BTN_SHOW_ALL}                    id=btnShowAll

${BTN_QUOTE_NO}                     css=a.noUnderline > strong
#${BTN_ACCEPT_QTY}                    id=btnAccept_186167
${BTN_ACCEPT_QTY}                    xpath=//input[@type="button" and @value="Accept"]
${QUOTE_NEW_WINDOW}                  name=acceptQuote
${QUOTE_CALANDER}                 id=tcalico_1

${QUOTE_CALANDER_DATE}               //td[@onclick="A_TCALS['1'].f_hide(1470767400000)"]
${TXT_PO_NUMBER}             id=txtPONum
${TXT_QUOTE_COMMENT}                       id=txtComment
${CB_QOUTE_TC}                   id=chkTC
${BTN_QUOTE_ACCEPT}                id=btnAccept



*** Keywords ***
SW-PRINTMANAGE-QUOTE-Note_down_the_job_number_to_track_your_oder
    wait until page contains  Thank you for accepting Quote!

SW-PRINTMANAGE-QUOTE-Click_on_Accept_to_place_the_order
    click element    ${BTN_QUOTE_ACCEPT}
    confirm action

SW-PRINTMANAGE-QUOTE-Enter-job_order_details_and_agree_to_the_Terms_Conditions
    SW-PRINTMANAGE-QUOTE-enter_po_no    123
    SW-PRINTMANAGE-QUOTE-enter_comment    test
    click element    ${CB_QOUTE_TC}

SW-PRINTMANAGE-QUOTE-enter_comment
    [Arguments]    ${com}
    input text    ${TXT_QUOTE_COMMENT}     ${com}

SW-PRINTMANAGE-QUOTE-enter_po_no
    [Arguments]    ${pono}
    input text    ${TXT_PO_NUMBER}     ${pono}




SW-PRINTMANAGE-QUOTE-Select_quantity_you_wish_to_accept_and_click_Accept_RFQ
    click element    ${BTN_ACCEPT_QTY}
    select window    ${QUOTE_NEW_WINDOW}
    click element    ${QUOTE_CALANDER}
    click element    ${QUOTE_CALANDER_DATE}


SW-PRINTMANAGE-QUOTE-Select_the_Quote_No
    #page should not contain    NO QUOTES FOUND
    click element     ${BTN_QUOTE_NO}

SW-PRINTMANAGE-QUOTE-Select_quote_and_click_View
    click element    ${BTN_SHOW_ALL}
    click link    ${LINK_VIEW}
