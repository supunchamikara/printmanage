*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
Library           Collections
Library           String

*** Variables ***



*** Keywords ***

SW-PRINTMANAGE-REPORT-STOCK-ON_ORDER-Make_sure_that_stock_on_order_Report_is_displayed
    wait until page contains     BLUE WATER - STOCK ON ORDER