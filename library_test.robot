*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Resource          PrintManage-Common/PrintManage-Common.robot
Resource          PrintManage-Pages/SW-PrintManage-LoginPage.robot
Resource          PrintManage-Pages/SW-PrintManage-HomePage.robot
Resource          PrintManage-Pages/SW-PrintManage-DashboardPage.robot
Resource          PrintManage-Pages/SW-PrintManage-PrintManagementPage.robot
Resource          PrintManage-Pages/SW-PrintManage-SummeryPage.robot
Resource          PrintManage-Pages/SW-PrintManage-OrderPage.robot
Resource          PrintManage-Pages/SW-PrintManage-CartViewPage.robot
Resource          PrintManage-Pages/SW-PrintManage-DespachDocketPage.robot
Resource          PrintManage-Pages/SW-PrintManage-QuoteRequestPage.robot
Resource          PrintManage-Pages/SW-PrintManage-QuotePage.robot
Resource          PrintManage-Pages/SW-PrintManagement-CreateQuoteFromQuotePage.robot
Resource          PrintManage-Pages/SW-PrintManage-JobsPage.robot
Resource          PrintManage-Pages/SW-PrintManage-StockPage.robot
Resource          PrintManage-Pages/SW-PrintManage-PrintPage.robot
Resource          PrintManage-Pages/SW-PrintManage-UsagePage.robot
Resource          PrintManage-Pages/SW-PrintManage-ReportJobPage.robot
Resource          PrintManage-Pages/SW-PrintManage-ReportOrderPage.robot
Resource          PrintManage-Pages/SW-PrintManage-ReporSSRPage.robot
Resource          PrintManage-Pages/SW-PrintManage-DeliverAddressPage.robot
Resource          PrintManage-Pages/SW-PrintManage-OrderACPage.robot
Resource          PrintManage-Pages/SW-PrintManage-OrderCDPage.robot
Resource          PrintManage-Pages/SW-PrintManage-OrderUAPage.robot
Resource          PrintManage-Pages/SW-PrintManage-ReportQuotePage.robot
Resource          PrintManage-Pages/SW-PrintManage-ReportStockPage.robot
Resource          PrintManage-Pages/SW-PrintManage-ReportStockUsagePage.robot
Resource          PrintManage-Pages/SW-PrintManage-ReportStockOnOrederPage.robot
Resource          PrintManage-Pages/SW-PrintManage-ReportEBPPage.robot
#Library           ReusableModule
Library           Selenium2Library
Library           Collections
Library           String


*** Variables ***




*** Keywords ***
























