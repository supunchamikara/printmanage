*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
Library           Collections
Library           String
Library           OperatingSystem

*** Variables ***
${SERVER}                   https://uat-portal.fxdms.net/sc/
${DOMAIN}                   https://uat-portal.fxdms.net
${BROWSER}                  chrome
${DELAY}                    1
#${DESIRED_CAPABILITIES}     browserName:internet explorer,version:11.545.10586.0
${DESIRED_CAPABILITIES}     browserName:chrome,version:54.0.2840.71
#${DESIRED_CAPABILITIES}     browserName:firefox,version:49.0.1
${REMOTE_URL}                http://192.168.1.100:4444/wd/hub



*** Keywords ***
PM-Close all browsers
    Create File  ${EXECDIR}/reddy.rdy
    Create Directory    last_result
    close all browsers

Create_screenshots_folder
    #${foo}=    Get Time    year month day hour minute second
    #_${BROWSER}_${foo[0]}-${foo[1]}-${foo[2]} ${foo[3]}-${foo[4]}-${foo[5]}
    ${folderName}=    Set Variable   screenshots
    Set Global Variable      ${folderName}
    Create Directory    ${folderName}

Save Screenshot
    [Arguments]    ${testnumber}    ${name}
    ${foo}=    Get Time    year month day hour minute second
    #${uuid} =  Generate Random String  6  [UPPER]
    Create_screenshots_folder
    Capture Page Screenshot  ${folderName}/${BROWSER}-${testnumber}-${name}.png


