*** Settings ***
Documentation     A test suite with a single test for valid login.
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          library_test.robot
Test Teardown     close all browsers
suite teardown    PM-Close all browsers


*** Test Cases ***
PRMGT-JOB004-Verify 'Held Jobs' view
    SW-PRINTMANAGE_LOGIN-Log_into_the_Print_Management_Portal
	Save Screenshot    PRMGT-JOB004    Log_into_the_Print_Management_Portal
	SW-PRINTMANAGE-LOGIN-Sign_in
	Save Screenshot    PRMGT-JOB004    Sign_in
	
PRMGT-ORD010-Verify if user can submit Call Off Orders
	SW-PRINTMANAGE_LOGIN-Log_into_the_Print_Management_Portal
	Save Screenshot    PRMGT-ORD010    Log_into_the_Print_Management_Portal
	SW-PRINTMANAGE-LOGIN-Sign_in
	Save Screenshot    PRMGT-ORD010    Sign_in
	SW-PRINTMANAGE-DASHBOARD-Go_to_print_management_page
	Save Screenshot    PRMGT-ORD010    Go_to_print_management_page
	SW-PRINTMANAGE-PM-Select_Customer_then_select_'Test Rep'_as_Sales_Rep
	Save Screenshot    PRMGT-ORD010    Select_Customer_then_select_Test Rep_as_Sales_Rep
	SW-PRINTMANAGE-PM-Select_Blue_Water_as_client
	Save Screenshot    PRMGT-ORD010    Select_Blue_Water_as_client
	SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_on_Ordering
	Save Screenshot    PRMGT-ORD010    From_the_navigation_menu_click_on_Ordering
	SW-PRINTMANAGE-SUMMERY-From_the_navigation_menu_click_oncall_of_Ordering
	Save Screenshot    PRMGT-ORD010    From_the_navigation_menu_click_oncall_of_Ordering
	SW-PRINTMANAGE-ORDER-AC-Call_Off_Order_list_will_be_displayed
	Save Screenshot    PRMGT-ORD010    Call_Off_Order_list_will_be_displayed
	SW-PRINTMANAGE-ORDER-AC-Select_and_click_the_order_you_wish_to_display
	Save Screenshot    PRMGT-ORD010    Select_and_click_the_order_you_wish_to_display
	SW-PRINTMANAGE-ORDER-Select_and_click_the_order_you_wish_to_copy_to_Shopping_Cart
	Save Screenshot    PRMGT-ORD010    Select_and_click_the_order_you_wish_to_copy_to_Shopping_Cart
	SW-PRINTMANAGE-CARTVIEW-Enter_the_delivery_information_and_click_on_Submit_Shopping_Cart
    Save Screenshot    PRMGT-ORD010    Enter_the_delivery_information_and_click_on_Submit_Shopping_Cart
	