ECHO OFF
:: executing robot script
::start Pybot.bat "%~dp0\testcases_printmanage.robot"
set folderName=%date:~7,2%-%date:~4,2%-%date:~10,4%_%time:~0,2%-%time:~3,2%-%time:~6,2%
:while1
    If Exist "%~dp0\reddy.rdy" (
        md "%~dp0%folderName%"
	sleep 7
	move "%~dp0\screenshots" "%~dp0\%folderName%\screenshots"
	move "%~dp0\log.html" "%~dp0\%folderName%\log.html"
	move "%~dp0\report.html" "%~dp0\%folderName%\report.html"
	del "%~dp0\reddy.rdy"
        goto :Exit
     )Else (
         goto :while1
     )