import win32gui
import win32con

class ReusableModule(object):
    def certificate_continue(self):
        """
        Find the IE Window that has a Certificate Error and try to continue anyway.
        We'll use the win32 modules to find the right window & child window,
        then write some Javascript into the address bar and execute to continue.
        """

        def _enumWindowsCallback(hwnd, windows):
            """
            This appends window information as a 3-tuple to the list
            passed into win32gui.EnumWindows()
            """
            class_name = win32gui.GetClassName(hwnd)
            # apparently win32gui.GetWindowText() only works to get the text
            # on a button or a label not really for edit windows.
            text = win32gui.GetWindowText(hwnd)
            windows.append((hwnd, class_name, text))

        def _get_certificate_error_window():
            """
            all_windows[] gets filled up with a list of tuples, then loop through
            it filtering on class and the window text (title bar text).
            Assumes only one 'Certificate Error' window.
            """
            all_windows = []
            win32gui.EnumWindows(_enumWindowsCallback, all_windows)
            for win in all_windows:
                class_name = win[1]
                title_bar_text = win[2]
                if class_name == 'IEFrame' and \
                                'Certificate Error: Navigation Blocked' in title_bar_text:
                    return win

        def _get_edit_text(hwnd):
            buf_size = win32gui.SendMessage(hwnd, win32con.WM_GETTEXTLENGTH, 0, 0)
            buf_size += 1  # don't forget that null character boys...
            buffer = win32gui.PyMakeBuffer(buf_size)
            win32gui.SendMessage(hwnd, win32con.WM_GETTEXT, buf_size, buffer)
            # don't need the null character now for Python
            return buffer[:buf_size]

        def _get_address_bar(parent_handle):
            """
            There appears to be several 'Edit' windows within each browser window.
            From Microsoft: If a child window has created child windows of its own,
            EnumChildWindows enumerates those windows as well.
            """
            childwins = []
            win32gui.EnumChildWindows(parent_handle, _enumWindowsCallback,
                                      childwins)
            for win in childwins:
                child_handle = win[0]
                class_name = win[1]
                if 'Edit' in class_name:
                    edit_text = _get_edit_text(child_handle)
                    if 'http://' in edit_text or 'https://' in edit_text:
                        return child_handle  # then this must be it...

                        # begin certificate_continue

        target_win = _get_certificate_error_window()
        try:
            cert_err_handle = target_win[0]
        except TypeError:
            print "OK, no Certificate Error window available"
            return (1)

        address_bar_handle = _get_address_bar(cert_err_handle)
        # any better way to check the handle ?
        if not win32gui.IsWindow(address_bar_handle):
            print "Choked getting IE edit window"
            return (1)

        # now, need to send this JavaScript text to the browser Address Bar
        javascript_continue = 'javascript: var continue_element = document.getElementById("overridelink"); continue_element.click();'
        win32gui.SendMessage(address_bar_handle, win32con.WM_SETTEXT, 0,
                             javascript_continue)

        # OK, and finally, send a carriage return to the address bar
        win32gui.SendMessage(address_bar_handle, win32con.WM_KEYDOWN,
                             win32con.VK_RETURN, 0)
        return (0)

